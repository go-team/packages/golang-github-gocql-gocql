Source: golang-github-gocql-gocql
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Sascha Steinbiss <satta@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-golang-snappy-dev,
               golang-github-hailocab-go-hostpool-dev,
               golang-gopkg-inf.v0-dev,
               golang-github-pierrec-lz4-dev,
               golang-github-stretchr-testify-dev
Standards-Version: 4.6.2
Homepage: https://github.com/gocql/gocql
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-gocql-gocql
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-gocql-gocql.git
XS-Go-Import-Path: github.com/gocql/gocql
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-go

Package: golang-github-gocql-gocql-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-golang-snappy-dev,
         golang-github-hailocab-go-hostpool-dev,
         golang-gopkg-inf.v0-dev,
         golang-github-pierrec-lz4-dev
Description: fast and robust Cassandra client for Go
 gocql implements a fast and robust Cassandra client for the Go
 programming language. It supports the following features:
 .
   * cluster management
   * automatic and safe type conversion between Cassandra and Go without any
     loss of precision
   * synchronous API with an asynchronous and concurrent back-end
   * result paging
   * atomic batch execution
   * query tracing to obtain a detailed output of all events that happened
      during the query execution
   * frame compression
   * support of multiple Cassandra versions
